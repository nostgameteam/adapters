namespace UnityEngine
{
    /// <summary>
    /// <inheritdoc/>
    /// Adapter class for Unity <see cref="Object"/>.
    /// </summary>
    public class ObjectAdapter : IObject
    {
        public string Name
        {
            get => obj.name;
            set => obj.name = value;
        }

        public HideFlags HideFlags
        {
            get => obj.hideFlags;
            set => obj.hideFlags = value;
        }

        private readonly Object obj;

        public ObjectAdapter(Object obj) => this.obj = obj;

        public int GetInstanceID() => obj.GetInstanceID();
    }
}