namespace UnityEngine
{
    /// <summary>
    /// Provider interface for a <see cref="Component"/>.
    /// </summary>
    public interface IComponent : IObject
    {
        /// <summary>
        /// <inheritdoc cref="Component.transform"/>
        /// </summary>
        Transform Transform { get; }

        /// <summary>
        /// <inheritdoc cref="Component.gameObject"/>
        /// </summary>
        GameObject GameObject { get; }

        /// <summary>
        /// <inheritdoc cref="Component.tag"/>
        /// </summary>
        string Tag { get; set; }

        /// <summary>
        /// <inheritdoc cref="Component.CompareTag(string)"/>
        /// </summary>
        bool CompareTag(string tag);

        /// <summary>
        /// Gets the component of the specified type, if it exists.
        /// </summary>
        /// <typeparam name="T">The type of the component to retrieve.</typeparam>
        /// <param name="component">The output argument that will contain the component or null.</param>
        /// <returns>Returns true if the component is found, false otherwise.</returns>
        bool TryGetComponent<T>(out T component);

        /// <summary>
        /// <inheritdoc cref="Component.GetComponent(System.Type)"/>
        /// </summary>
        T GetComponent<T>();

        /// <summary>
        /// <inheritdoc cref="Component.GetComponents(System.Type)"/>
        /// </summary>
        T[] GetComponents<T>();

        /// <summary>
        /// <inheritdoc cref="Component.GetComponentInParent(System.Type)"/>
        /// </summary>
        T GetComponentInParent<T>();

        /// <summary>
        /// <inheritdoc cref="Component.GetComponentsInParent(System.Type, bool)"/>
        /// </summary>
        T[] GetComponentsInParent<T>(bool includeInactive = false);

        /// <summary>
        /// <inheritdoc cref="Component.GetComponentInChildren(System.Type)"/>
        /// </summary>
        T GetComponentInChildren<T>(bool includeInactive = false);

        /// <summary>
        /// <inheritdoc cref="Component.GetComponentsInChildren(System.Type, bool)"/>
        /// </summary>
        T[] GetComponentsInChildren<T>(bool includeInactive = false);
    }
}