using System.Collections;

namespace UnityEngine
{
    /// <summary>
    /// Provider interface for <see cref="Transform"/>.
    /// </summary>
    public interface ITransform : IComponent, IEnumerable
    {
        #region PROPERTIES
        /// <summary>
        /// <inheritdoc cref="Transform.localPosition"/>
        /// </summary>       
        Vector3 LocalPosition { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.eulerAngles"/>
        /// </summary>   
        Vector3 EulerAngles { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.localEulerAngles"/>
        /// </summary>   
        Vector3 LocalEulerAngles { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.right"/>
        /// </summary>   
        Vector3 Right { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.up"/>
        /// </summary>   
        Vector3 Up { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.forward"/>
        /// </summary>   
        Vector3 Forward { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.rotation"/>
        /// </summary>   
        Quaternion Rotation { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.position"/>
        /// </summary>   
        Vector3 Position { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.localRotation"/>
        /// </summary>   
        Quaternion LocalRotation { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.parent"/>
        /// </summary>   
        Transform Parent { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.worldToLocalMatrix"/>
        /// </summary>   
        Matrix4x4 WorldToLocalMatrix { get; }

        /// <summary>
        /// <inheritdoc cref="Transform.localToWorldMatrix"/>
        /// </summary>   
        Matrix4x4 LocalToWorldMatrix { get; }

        /// <summary>
        /// <inheritdoc cref="Transform.root"/>
        /// </summary>   
        Transform Root { get; }

        /// <summary>
        /// <inheritdoc cref="Transform.childCount"/>
        /// </summary>   
        int ChildCount { get; }

        /// <summary>
        /// <inheritdoc cref="Transform.lossyScale"/>
        /// </summary>   
        Vector3 LossyScale { get; }

        /// <summary>
        /// <inheritdoc cref="Transform.localScale"/>
        /// </summary>   
        Vector3 LocalScale { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.hierarchyCapacity"/>
        /// </summary>   
        int HierarchyCapacity { get; set; }

        /// <summary>
        /// <inheritdoc cref="Transform.hierarchyCount"/>
        /// </summary>   
        int HierarchyCount { get; }
        #endregion

        #region FUNCTIONS
        /// <summary>
        /// <inheritdoc cref="Transform.Translate(float, float, float, Space)"/>
        /// </summary>
        void Translate(float x, float y, float z, Space relativeTo = Space.Self);

        /// <summary>
        /// <inheritdoc cref="Transform.Translate(Vector3, Space)"/>
        /// </summary>
        void Translate(Vector3 translation, Space relativeTo = Space.Self);

        /// <summary>
        /// <inheritdoc cref="Transform.Translate(float, float, float, Transform)"/>
        /// </summary>
        void Translate(float x, float y, float z, Transform relativeTo);

        /// <summary>
        /// <inheritdoc cref="Transform.Translate(Vector3, Transform)"/>
        /// </summary>
        void Translate(Vector3 translation, Transform relativeTo);

        /// <summary>
        /// <inheritdoc cref="Transform.SetAsFirstSibling"/>
        /// </summary>
        void SetAsFirstSibling();

        /// <summary>
        /// <inheritdoc cref="Transform.SetAsLastSibling"/>
        /// </summary>
        void SetAsLastSibling();

        /// <summary>
        /// <inheritdoc cref="Transform.SetParent(Transform)"/>
        /// </summary>
        void SetParent(Transform p);

        /// <summary>
        /// <inheritdoc cref="Transform.SetParent(Transform, bool)"/>
        /// </summary>
        void SetParent(Transform parent, bool worldPositionStays);

        /// <summary>
        /// <inheritdoc cref="Transform.SetPositionAndRotation(Vector3, Quaternion)"/>
        /// </summary>
        void SetPositionAndRotation(Vector3 position, Quaternion rotation);

        /// <summary>
        /// <inheritdoc cref="Transform.SetSiblingIndex(int)"/>
        /// </summary>
        void SetSiblingIndex(int index);

        /// <summary>
        /// <inheritdoc cref="Transform.Rotate(Vector3, Space)"/>
        /// </summary>
        void Rotate(Vector3 eulers, Space relativeTo = Space.Self);

        /// <summary>
        /// <inheritdoc cref="Transform.Rotate(float, float, float, Space)"/>
        /// </summary>
        void Rotate(float xAngle, float yAngle, float zAngle, Space relativeTo = Space.Self);

        /// <summary>
        /// <inheritdoc cref="Transform.Rotate(Vector3, float, Space)"/>
        /// </summary>
        void Rotate(Vector3 axis, float angle, Space relativeTo = Space.Self);

        /// <summary>
        /// <inheritdoc cref="Transform.RotateAround(Vector3, Vector3, float))"/>
        /// </summary>
        void RotateAround(Vector3 point, Vector3 axis, float angle);

        /// <summary>
        /// <inheritdoc cref="Transform.LookAt(Transform)"/>
        /// </summary>
        void LookAt(Transform target);

        /// <summary>
        /// <inheritdoc cref="Transform.LookAt(Transform, Vector3)"/>
        /// </summary>
        void LookAt(Transform target, Vector3 worldUp);

        /// <summary>
        /// <inheritdoc cref="Transform.LookAt(Vector3)"/>
        /// </summary>
        void LookAt(Vector3 worldPosition);

        /// <summary>
        /// <inheritdoc cref="Transform.LookAt(Vector3, Vector3)"/>
        /// </summary>
        void LookAt(Vector3 worldPosition, Vector3 worldUp);

        /// <summary>
        /// <inheritdoc cref="Transform.DetachChildren"/>
        /// </summary>
        void DetachChildren();

        /// <summary>
        /// <inheritdoc cref="Transform.GetChild(int)"/>
        /// </summary>
        Transform GetChild(int index);

        /// <summary>
        /// <inheritdoc cref="Transform.GetSiblingIndex"/>
        /// </summary>
        int GetSiblingIndex();

        /// <summary>
        /// <inheritdoc cref="Transform.IsChildOf(Transform)"/>
        /// </summary>
        bool IsChildOf(Transform parent);
        #endregion
    }
}