using System.Collections;

namespace UnityEngine
{
    /// <summary>
    /// Adapter class for <see cref="Transform"/>.
    /// </summary>
    public sealed class TransformAdapter : ComponentAdapter, ITransform
    {
        #region PROPERTIES
        public Vector3 Position { get => transform.position; set => transform.position = value; }
        public Vector3 LocalPosition { get => transform.localPosition; set => transform.localPosition = value; }
        public Vector3 EulerAngles { get => transform.eulerAngles; set => transform.eulerAngles = value; }
        public Vector3 LocalEulerAngles { get => transform.localEulerAngles; set => transform.localEulerAngles = value; }
        public Vector3 LocalScale { get => transform.localScale; set => transform.localScale = value; }
        public Vector3 LossyScale => transform.lossyScale;
        public Vector3 Up { get => transform.up; set => transform.up = value; }
        public Vector3 Right { get => transform.right; set => transform.right = value; }
        public Vector3 Forward { get => transform.forward; set => transform.forward = value; }
        public Quaternion Rotation { get => transform.rotation; set => transform.rotation = value; }
        public Quaternion LocalRotation { get => transform.localRotation; set => transform.localRotation = value; }

        public Transform Root => transform.root;
        public Transform Parent { get => transform.parent; set => transform.parent = value; }

        public Matrix4x4 WorldToLocalMatrix => transform.worldToLocalMatrix;
        public Matrix4x4 LocalToWorldMatrix => transform.localToWorldMatrix;

        public int ChildCount => transform.childCount;
        public int HierarchyCount => transform.hierarchyCount;
        public int HierarchyCapacity { get => transform.hierarchyCapacity; set => transform.hierarchyCapacity = value; }
        #endregion

        private readonly Transform transform;
        public TransformAdapter(Transform transform) :
            base(transform) =>
            this.transform = transform;

        #region FUNCTIONS
        public void Translate(float x, float y, float z, Space relativeTo = Space.Self) => transform.Translate(x, y, z, relativeTo);
        public void Translate(Vector3 translation, Space relativeTo = Space.Self) => transform.Translate(translation, relativeTo);
        public void Translate(float x, float y, float z, Transform relativeTo) => transform.Translate(x, y, z, relativeTo);
        public void Translate(Vector3 translation, Transform relativeTo) => transform.Translate(translation, relativeTo);

        public void SetAsFirstSibling() => transform.SetAsFirstSibling();
        public void SetAsLastSibling() => transform.SetAsLastSibling();
        public void SetParent(Transform p) => transform.SetParent(p);
        public void SetParent(Transform parent, bool worldPositionStays) => transform.SetParent(parent, worldPositionStays);
        public void SetPositionAndRotation(Vector3 position, Quaternion rotation) => transform.SetPositionAndRotation(position, rotation);
        public void SetSiblingIndex(int index) => transform.SetSiblingIndex(index);

        public void Rotate(Vector3 eulers, Space relativeTo = Space.Self) => transform.Rotate(eulers, relativeTo);
        public void Rotate(float xAngle, float yAngle, float zAngle, Space relativeTo = Space.Self) =>
            transform.Rotate(xAngle, yAngle, zAngle, relativeTo);
        public void Rotate(Vector3 axis, float angle, Space relativeTo = Space.Self) => transform.Rotate(axis, angle, relativeTo);
        public void RotateAround(Vector3 point, Vector3 axis, float angle) => transform.RotateAround(point, axis, angle);

        public void LookAt(Transform target) => transform.LookAt(target);
        public void LookAt(Transform target, Vector3 worldUp) => transform.LookAt(target, worldUp);
        public void LookAt(Vector3 worldPosition) => transform.LookAt(worldPosition);
        public void LookAt(Vector3 worldPosition, Vector3 worldUp) => transform.LookAt(worldPosition, worldUp);

        public void DetachChildren() => transform.DetachChildren();
        public Transform GetChild(int index) => transform.GetChild(index);

        public int GetSiblingIndex() => transform.GetSiblingIndex();
        public bool IsChildOf(Transform parent) => transform.IsChildOf(parent);

        public IEnumerator GetEnumerator() => transform.GetEnumerator();
        #endregion
    }
}