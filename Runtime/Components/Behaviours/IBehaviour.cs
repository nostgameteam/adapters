namespace UnityEngine
{
    /// <summary>
    /// Provider interface for <see cref="Behaviour"/>.
    /// </summary>
    public interface IBehaviour : IComponent
    {
        /// <summary>
        /// <inheritdoc cref="Behaviour.enabled"/>
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// <inheritdoc cref="Behaviour.isActiveAndEnabled"/>
        /// </summary>
        bool IsActiveAndEnabled { get; }
    }
}