namespace UnityEngine
{
    /// <summary>
    /// <inheritdoc/>
    /// Adapter class for Unity <see cref="Behaviour"/>
    /// </summary>
    public class BehaviourAdapter : ComponentAdapter, IBehaviour
    {
        public bool IsActiveAndEnabled => behaviour.isActiveAndEnabled;
        public bool Enabled { get => behaviour.enabled; set => behaviour.enabled = value; }

        private readonly Behaviour behaviour;
        public BehaviourAdapter(Behaviour behaviour) :
            base(behaviour) =>
            this.behaviour = behaviour;
    }
}