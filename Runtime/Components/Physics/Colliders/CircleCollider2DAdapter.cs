namespace UnityEngine
{
    /// <summary>
    /// Adapter class for Unity <see cref="CircleCollider2D"/>.
    /// </summary>
    public sealed class CircleCollider2DAdapter : ComponentAdapter, ICollider
    {
        public bool Enabled { get => collider.enabled; set => collider.enabled = value; }
        public bool IsTrigger { get => collider.isTrigger; set => collider.isTrigger = value; }
        public float Radius { get => collider.radius; set => collider.radius = value; }

        public Bounds Bounds => collider.bounds;
        public Vector3 Size { get => collider.bounds.size; set => collider.radius = value.magnitude; }
        public Vector3 Center { get => collider.offset; set => collider.offset = value; }

        private readonly CircleCollider2D collider;
        public CircleCollider2DAdapter(CircleCollider2D collider) :
            base(collider) =>
            this.collider = collider;

        public Vector3 ClosestPoint(Vector3 position) => collider.ClosestPoint(position);
    }
}