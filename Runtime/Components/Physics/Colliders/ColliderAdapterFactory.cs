namespace UnityEngine
{
    /// <summary>
    /// Factory class for <see cref="ICollider"/>.
    /// </summary>
    public static class ColliderAdapterFactory
    {
        /// <summary>
        /// Creates a <see cref="BoxColliderAdapter"/>, <see cref="BoxCollider2DAdapter"/>,
        /// <see cref="SphereColliderAdapter"/> or <see cref="CircleCollider2DAdapter"/>
        /// depending on the Collider added to the given gameObject.
        /// <para>Returns null if no Collider is present on the given gameObject.</para>
        /// </summary>
        /// <param name="gameObject">The game object to fetch a Collider.</param>
        /// <returns>
        /// An instance of <see cref="ICollider"/> or <b>null</b>.
        /// </returns>
        public static ICollider Create(GameObject gameObject)
        {
            if (gameObject.TryGetComponent(out BoxCollider boxCollider))
                return new BoxColliderAdapter(boxCollider);
            else if (gameObject.TryGetComponent(out BoxCollider2D boxCollider2D))
                return new BoxCollider2DAdapter(boxCollider2D);
            else if (gameObject.TryGetComponent(out SphereCollider sphereCollider))
                return new SphereColliderAdapter(sphereCollider);
            else if (gameObject.TryGetComponent(out CircleCollider2D circleCollider))
                return new CircleCollider2DAdapter(circleCollider);

            Debug.LogError($"No Collider or Collider2D found on {gameObject.name}. You need to add one.");
            return null;
        }
    }
}