namespace UnityEngine
{
    /// <summary>
    /// Adapter class for Unity <see cref="BoxCollider"/>.
    /// </summary>
    public sealed class BoxColliderAdapter : ComponentAdapter, ICollider
    {
        public bool Enabled { get => collider.enabled; set => collider.enabled = value; }
        public bool IsTrigger { get => collider.isTrigger; set => collider.isTrigger = value; }

        public Bounds Bounds => collider.bounds;
        public Vector3 Size { get => collider.size; set => collider.size = value; }
        public Vector3 Center { get => collider.center; set => collider.center = value; }

        private readonly BoxCollider collider;
        public BoxColliderAdapter(BoxCollider collider) :
            base(collider) =>
            this.collider = collider;

        public Vector3 ClosestPoint(Vector3 position) => collider.ClosestPoint(position);
    }
}