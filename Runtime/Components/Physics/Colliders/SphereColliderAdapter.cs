namespace UnityEngine
{
    /// <summary>
    /// Adapter class for Unity <see cref="SphereCollider"/>.
    /// </summary>
    public sealed class SphereColliderAdapter : ComponentAdapter, ICollider
    {
        public bool Enabled { get => collider.enabled; set => collider.enabled = value; }
        public bool IsTrigger { get => collider.isTrigger; set => collider.isTrigger = value; }
        public float Radius { get => collider.radius; set => collider.radius = value; }

        public Bounds Bounds => collider.bounds;
        public Vector3 Size { get => collider.bounds.size; set => collider.radius = value.magnitude; }
        public Vector3 Center { get => collider.center; set => collider.center = value; }

        private readonly SphereCollider collider;
        public SphereColliderAdapter(SphereCollider collider) :
            base(collider) =>
            this.collider = collider;

        public Vector3 ClosestPoint(Vector3 position) => collider.ClosestPoint(position);
    }
}