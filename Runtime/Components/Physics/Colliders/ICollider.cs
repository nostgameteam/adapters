namespace UnityEngine
{
    /// <summary>
    /// Provider interface for a <see cref="Collider"/>.
    /// </summary>
    public interface ICollider : IComponent
    {
        /// <summary>
        /// <inheritdoc cref="Collider.enabled"/>
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// <inheritdoc cref="Collider.isTrigger"/>
        /// </summary>
        bool IsTrigger { get; set; }

        /// <summary>
        /// <inheritdoc cref="Collider.bounds"/>
        /// </summary>
        Bounds Bounds { get; }

        /// <summary>
        /// The size of the collider, measured in the object's local space.
        /// </summary>
        Vector3 Size { get; set; }

        /// <summary>
        /// The center of the collider, measured in the object's local space.
        /// </summary>
        Vector3 Center { get; set; }

        /// <summary>
        /// <inheritdoc cref="Collider.ClosestPoint(Vector3)"/>
        /// </summary>
        Vector3 ClosestPoint(Vector3 position);
    }
}