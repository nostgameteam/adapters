namespace UnityEngine
{
    /// <summary>
    /// Adapter class for Unity <see cref="BoxCollider2D"/>.
    /// </summary>
    public sealed class BoxCollider2DAdapter : ComponentAdapter, ICollider
    {
        public bool Enabled { get => collider.enabled; set => collider.enabled = value; }
        public bool IsTrigger { get => collider.isTrigger; set => collider.isTrigger = value; }

        public Bounds Bounds => collider.bounds;
        public Vector3 Size { get => collider.size; set => collider.size = value; }
        public Vector3 Center { get => collider.offset; set => collider.offset = value; }

        private readonly BoxCollider2D collider;
        public BoxCollider2DAdapter(BoxCollider2D collider) :
            base(collider) =>
            this.collider = collider;

        public Vector3 ClosestPoint(Vector3 position) => collider.ClosestPoint(position);
    }
}