namespace UnityEngine
{
    /// <summary>
    /// Factory class for <see cref="IRigidbody"/>.
    /// </summary>
    public static class RigidbodyAdapterFactory
    {
        /// <summary>
        /// Creates a <see cref="RigidbodyAdapter"/> or <see cref="Rigidbody2DAdapter"/>
        /// depending on the Rigidbody added to the given gameObject.
        /// <para>Returns null if no Rigidbody is present on the given gameObject.</para>
        /// </summary>
        /// <param name="gameObject">The game object to fetch a Rigidbody.</param>
        /// <returns>
        /// An instance of <see cref="RigidbodyAdapter"/>, 
        /// <see cref="Rigidbody2DAdapter"/> or <b>null</b>.
        /// </returns>
        public static IRigidbody Create(GameObject gameObject)
        {
            if (gameObject.TryGetComponent(out Rigidbody rigidbody))
                return new RigidbodyAdapter(rigidbody);
            else if (gameObject.TryGetComponent(out Rigidbody2D rigidbody2D))
                return new Rigidbody2DAdapter(rigidbody2D);

            Debug.LogError($"No Rigidbody or Rigidbody2D found on {gameObject.name}. You need to add one.");
            return null;
        }
    }
}