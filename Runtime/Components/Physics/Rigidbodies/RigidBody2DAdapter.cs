namespace UnityEngine
{
    /// <summary>
    /// <inheritdoc/>
    /// Adapter class for Unity <see cref="Rigidbody2D"/>
    /// </summary>
    public sealed class Rigidbody2DAdapter : ComponentAdapter, IRigidbody
    {
        public float Drag { get => body.drag; set => body.drag = value; }
        public float AngularDrag { get => body.angularDrag; set => body.angularDrag = value; }
        public float Mass { get => body.mass; set => body.mass = value; }
        public bool IsKinematic { get => body.isKinematic; set => body.isKinematic = value; }
        public bool FreezeRotation { get => body.freezeRotation; set => body.freezeRotation = value; }

        public Vector3 Velocity { get => body.velocity; set => body.velocity = value; }
        public Vector3 CenterOfMass { get => body.centerOfMass; set => body.centerOfMass = value; }
        public Vector3 WorldCenterOfMass => body.worldCenterOfMass;
        public Vector3 Position { get => body.position; set => body.position = value; }

        private readonly Rigidbody2D body;
        public Rigidbody2DAdapter(Rigidbody2D body) :
            base(body) =>
            this.body = body;

        public void AddForce(Vector3 force) => body.AddForce(force);
        public void AddForceAtPosition(Vector3 force, Vector3 position) => body.AddForceAtPosition(force, position);
        public void AddRelativeForce(Vector3 force) => body.AddRelativeForce(force);
        public void MovePosition(Vector3 position) => body.MovePosition(position);
        public void MoveRotation(Quaternion rotation) => body.MoveRotation(rotation);
        public void Sleep() => body.Sleep();
        public void WakeUp() => body.WakeUp();

        public Vector3 GetPointVelocity(Vector3 worldPoint) => body.GetPointVelocity(worldPoint);
        public Vector3 GetRelativePointVelocity(Vector3 relativePoint) => body.GetRelativePointVelocity(relativePoint);

        public bool IsSleeping() => body.IsSleeping();
    }
}