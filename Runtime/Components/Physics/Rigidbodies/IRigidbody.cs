namespace UnityEngine
{
    /// <summary>
    /// Provider interface for a <see cref="Rigidbody2D"/>.
    /// </summary>
    public interface IRigidbody : IComponent
    {
        /// <summary>
        /// <inheritdoc cref="Rigidbody.drag"/>
        /// </summary>
        float Drag { get; set; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.angularDrag"/>
        /// </summary>
        float AngularDrag { get; set; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.mass"/>
        /// </summary>
        float Mass { get; set; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.isKinematic"/>
        /// </summary>
        bool IsKinematic { get; set; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.freezeRotation"/>
        /// </summary>
        bool FreezeRotation { get; set; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.centerOfMass"/>
        /// </summary>
        Vector3 CenterOfMass { get; set; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.worldCenterOfMass"/>
        /// </summary>
        Vector3 WorldCenterOfMass { get; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.position"/>
        /// </summary>
        Vector3 Position { get; set; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.velocity"/>
        /// </summary>
        Vector3 Velocity { get; set; }

        /// <summary>
        /// <inheritdoc cref="Rigidbody.AddForce(Vector3)"/>
        /// </summary>
        void AddForce(Vector3 force);

        /// <summary>
        /// <inheritdoc cref="Rigidbody.AddForceAtPosition(Vector3, Vector3)"/>
        /// </summary>
        void AddForceAtPosition(Vector3 force, Vector3 position);

        /// <summary>
        /// <inheritdoc cref="Rigidbody.AddRelativeForce(Vector3)"/>
        /// </summary>
        void AddRelativeForce(Vector3 force);

        /// <summary>
        /// <inheritdoc cref="Rigidbody.MovePosition(Vector3)"/>
        /// </summary>
        void MovePosition(Vector3 position);

        /// <summary>
        /// <inheritdoc cref="Rigidbody.MoveRotation(Quaternion)"/>
        /// </summary>
        void MoveRotation(Quaternion rot);

        /// <summary>
        /// <inheritdoc cref="Rigidbody.Sleep"/>
        /// </summary>
        void Sleep();

        /// <summary>
        /// <inheritdoc cref="Rigidbody.WakeUp"/>
        /// </summary>
        void WakeUp();

        /// <summary>
        /// <inheritdoc cref="Rigidbody.GetPointVelocity(Vector3)(Vector3)"/>
        /// </summary>
        Vector3 GetPointVelocity(Vector3 worldPoint);

        /// <summary>
        /// <inheritdoc cref="Rigidbody.GetRelativePointVelocity(Vector3)"/>
        /// </summary>
        Vector3 GetRelativePointVelocity(Vector3 relativePoint);

        /// <summary>
        /// <inheritdoc cref="Rigidbody.IsSleeping"/>
        /// </summary>
        bool IsSleeping();
    }
}