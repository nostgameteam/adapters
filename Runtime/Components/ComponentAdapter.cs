namespace UnityEngine
{
    /// <summary>
    /// <inheritdoc/>
    /// Adapter class for Unity <see cref="Component"/>
    /// </summary>
    public class ComponentAdapter : ObjectAdapter, IComponent
    {
        public string Tag
        {
            get => component.tag;
            set => component.tag = value;
        }

        public Transform Transform => component.transform;

        public GameObject GameObject => component.gameObject;

        private readonly Component component;

        public ComponentAdapter(Component component) :
            base(component) =>
            this.component = component;

        public bool CompareTag(string tag) => component.CompareTag(tag);

        public T GetComponent<T>() => component.GetComponent<T>();

        public bool TryGetComponent<T>(out T component) =>
            this.component.TryGetComponent<T>(out component);

        public T[] GetComponents<T>() => component.GetComponents<T>();

        public T GetComponentInChildren<T>(bool includeInactive = false) =>
            component.GetComponentInChildren<T>(includeInactive);

        public T[] GetComponentsInChildren<T>(bool includeInactive = false) =>
            component.GetComponentsInChildren<T>(includeInactive);

        public T GetComponentInParent<T>() => component.GetComponentInParent<T>();

        public T[] GetComponentsInParent<T>(bool includeInactive = false) =>
            component.GetComponentsInParent<T>(includeInactive);
    }
}