using UnityEngine.Audio;

namespace UnityEngine
{
    /// <summary>
    /// <inheritdoc/>
    /// Adapter class for Unity <see cref="AudioSource"/>
    /// </summary>
    public sealed class AudioSourceAdapter : BehaviourAdapter, IAudioSource
    {
        public bool IsPlaying { get => source.isPlaying; }
        public bool Loop { get => source.loop; set => source.loop = value; }
        public bool PlayOnAwake { get => source.playOnAwake; set => source.playOnAwake = value; }
        public bool IgnoreListenerPause { get => source.ignoreListenerPause; set => source.ignoreListenerPause = value; }
        public bool Spatialize { get => source.spatialize; set => source.spatialize = value; }
        public bool SpatializePostEffects { get => source.spatializePostEffects; set => source.spatializePostEffects = value; }
        public bool BypassEffects { get => source.bypassEffects; set => source.bypassEffects = value; }
        public bool BypassListenerEffects { get => source.bypassListenerEffects; set => source.bypassListenerEffects = value; }
        public bool BypassReverbZones { get => source.bypassReverbZones; set => source.bypassReverbZones = value; }
        public bool Mute { get => source.mute; set => source.mute = value; }

        public int Priority { get => source.priority; set => source.priority = value; }
        public int TimeSamples { get => source.timeSamples; set => source.timeSamples = value; }

        public float Time { get => source.time; set => source.time = value; }
        public float PanStereo { get => source.panStereo; set => source.panStereo = value; }
        public float SpatialBlend { get => source.spatialBlend; set => source.spatialBlend = value; }
        public float ReverbZoneMix { get => source.reverbZoneMix; set => source.reverbZoneMix = value; }
        public float DopplerLevel { get => source.dopplerLevel; set => source.dopplerLevel = value; }
        public float Spread { get => source.spread; set => source.spread = value; }
        public float MinDistance { get => source.minDistance; set => source.minDistance = value; }
        public float MaxDistance { get => source.maxDistance; set => source.maxDistance = value; }
        public float Pitch { get => source.pitch; set => source.pitch = value; }
        public float Volume { get => source.volume; set => source.volume = value; }

        public AudioClip Clip { get => source.clip; set => source.clip = value; }
        public AudioMixerGroup OutputAudioMixerGroup { get => source.outputAudioMixerGroup; set => source.outputAudioMixerGroup = value; }
        public AudioVelocityUpdateMode VelocityUpdateMode { get => source.velocityUpdateMode; set => source.velocityUpdateMode = value; }

        private readonly AudioSource source;
        public AudioSourceAdapter(AudioSource source) :
            base(source) =>
            this.source = source;

        public void Pause() => source.Pause();
        public void Play() => source.Play();
        public void PlayDelayed(float delay) => source.PlayDelayed(delay);
        public void PlayOneShot(AudioClip clip, float volumeScale = 1F) => source.PlayOneShot(clip, volumeScale);
        public void PlayScheduled(double time) => source.PlayScheduled(time);
        public void Stop() => source.Stop();
        public void UnPause() => source.UnPause();
        public void PlayClipAtPoint(AudioClip clip, Vector3 position, float volume = 1F) =>
            AudioSource.PlayClipAtPoint(clip, position, volume);
    }
}