using UnityEngine.Audio;

namespace UnityEngine
{
    /// <summary>
    /// Provider interface for <see cref="AudioSource"/>.
    /// </summary>
    public interface IAudioSource : IBehaviour
    {
        /// <summary>
        /// <inheritdoc cref="AudioSource.isPlaying"/>
        /// </summary>
        bool IsPlaying { get; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.loop"/>
        /// </summary>
        bool Loop { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.playOnAwake"/>
        /// </summary>
        bool PlayOnAwake { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.ignoreListenerPause"/>
        /// </summary>
        bool IgnoreListenerPause { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.spatialize"/>
        /// </summary>
        bool Spatialize { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.spatializePostEffects"/>
        /// </summary>
        bool SpatializePostEffects { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.bypassEffects"/>
        /// </summary>
        bool BypassEffects { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.bypassListenerEffects"/>
        /// </summary>
        bool BypassListenerEffects { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.bypassReverbZones"/>
        /// </summary>
        bool BypassReverbZones { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.mute"/>
        /// </summary>
        bool Mute { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.priority"/>
        /// </summary>
        int Priority { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.timeSamples"/>
        /// </summary>
        int TimeSamples { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.time"/>
        /// </summary>
        float Time { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.panStereo"/>
        /// </summary>
        float PanStereo { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.spatialBlend"/>
        /// </summary>
        float SpatialBlend { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.reverbZoneMix"/>
        /// </summary>
        float ReverbZoneMix { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.dopplerLevel"/>
        /// </summary>
        float DopplerLevel { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.spread"/>
        /// </summary>
        float Spread { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.minDistance"/>
        /// </summary>
        float MinDistance { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.maxDistance"/>
        /// </summary>
        float MaxDistance { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.pitch"/>
        /// </summary>
        float Pitch { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.volume"/>
        /// </summary>
        float Volume { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.clip"/>
        /// </summary>
        AudioClip Clip { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.outputAudioMixerGroup"/>
        /// </summary>
        AudioMixerGroup OutputAudioMixerGroup { get; set; }

        /// <summary>
        /// <inheritdoc cref="AudioSource.velocityUpdateMode"/>
        /// </summary>
        AudioVelocityUpdateMode VelocityUpdateMode { get; set; }

        /// <summary>
        /// <see cref="AudioSource.Pause"/>
        /// </summary>
        void Pause();

        /// <summary>
        /// <see cref="AudioSource.Play"/>
        /// </summary>
        void Play();

        /// <summary>
        /// <see cref="AudioSource.PlayDelayed(float)"/>
        /// </summary>
        void PlayDelayed(float delay);

        /// <summary>
        /// <see cref="AudioSource.PlayOneShot(AudioClip, float)"/>
        /// </summary>
        void PlayOneShot(AudioClip clip, float volumeScale = 1F);

        /// <summary>
        /// <see cref="AudioSource.PlayScheduled(double)"/>
        /// </summary>
        void PlayScheduled(double time);

        /// <summary>
        /// <see cref="AudioSource.Stop"/>
        /// </summary>
        void Stop();

        /// <summary>
        /// <see cref="AudioSource.UnPause"/>
        /// </summary>
        void UnPause();

        /// <summary>
        /// <see cref="AudioSource.PlayClipAtPoint(AudioClip, Vector3, float)"/>
        /// </summary>
        void PlayClipAtPoint(AudioClip clip, Vector3 position, float volume = 1F);
    }
}