using System.Collections;

namespace UnityEngine
{
    /// <summary>
    /// Adapter class for <see cref="Coroutine"/>.
    /// </summary>
    public sealed class CoroutineAdapter : ICoroutine
    {
        private readonly MonoBehaviour behaviour;

        public CoroutineAdapter(MonoBehaviour behaviour) => this.behaviour = behaviour;

        public Coroutine StartCoroutine(IEnumerator routine) => behaviour.StartCoroutine(routine);

        public void StopCoroutine(IEnumerator routine) => behaviour.StopCoroutine(routine);

        public void StopAllCoroutines() => behaviour.StopAllCoroutines();
    }
}