using System.Collections;

namespace UnityEngine
{
    /// <summary>
    /// Provider interface for a <see cref="Coroutine"/>.
    /// </summary>
    public interface ICoroutine
    {
        /// <summary>
        /// <inheritdoc cref="MonoBehaviour.StartCoroutine(IEnumerator)"/>
        /// </summary>
        Coroutine StartCoroutine(IEnumerator routine);

        /// <summary>
        /// <inheritdoc cref="MonoBehaviour.StopAllCoroutines"/>
        /// </summary>
        void StopAllCoroutines();

        /// <summary>
        /// <inheritdoc cref="MonoBehaviour.StopCoroutine(IEnumerator)"/>
        /// </summary>
        void StopCoroutine(IEnumerator routine);
    }
}