using System.Collections;

namespace UnityEngine
{
    /// <summary>
    /// Manual coroutine runner. 
    /// Use it inside Unit Tests to mock <see cref="ICoroutine"/>.
    /// <para>You can control the coroutine execution using <see cref="MoveNext"/> and <see cref="MoveToEnd"/>.</para>
    /// </summary>
    public sealed class ManualCoroutine : ICoroutine, IEnumerator
    {
        /// <summary>
        /// Whether the coroutine is running.
        /// </summary>
        public bool IsRunning => Routine != null;

        /// <summary>
        /// <inheritdoc cref="IEnumerator.Current"/>
        /// </summary>
        public object Current => Routine.Current;

        /// <summary>
        /// The last started routine.
        /// </summary>
        public IEnumerator Routine { get; private set; }

        /// <summary>
        /// Resets the last started routine.
        /// </summary>
        public void Reset() => Routine?.Reset();

        /// <summary>
        /// Moves the last started routine to the next frame.
        /// </summary>
        public bool MoveNext() => (Routine?.MoveNext()).GetValueOrDefault();

        /// <summary>
        /// Moves the last started routine to the end.
        /// <para>Use this method to finish the routine.</para>
        /// </summary>
        public void MoveToEnd()
        {
            while (Routine.MoveNext())
            {
                MoveToEnd(Routine.Current as IEnumerator);
            }
        }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            Routine = routine;
            return null;
        }

        public void StopCoroutine(IEnumerator routine)
        {
            if (Routine == routine) Routine = null;
        }

        public void StopAllCoroutines() => Routine = null;

        private void MoveToEnd(IEnumerator current)
        {
            while (current != null && current.MoveNext())
            {
                IEnumerator newEnumerator = Routine.Current as IEnumerator;
                if (newEnumerator != current)
                    MoveToEnd(newEnumerator);
            }
        }
    }
}