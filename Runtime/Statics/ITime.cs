using UnityTime = UnityEngine.Time;

namespace UnityEngine
{
    /// <summary>
    /// Provider interface for a <see cref="UnityEngine.Time"/>.
    /// </summary>
    public interface ITime
    {
        /// <summary>
        /// <inheritdoc cref="UnityTime.captureDeltaTime"/>
        /// </summary>
        float CaptureDeltaTime { get; set; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.realtimeSinceStartupAsDouble"/>
        /// </summary>
        double RealtimeSinceStartupAsDouble { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.realtimeSinceStartup"/>
        /// </summary>
        float RealtimeSinceStartup { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.renderedFrameCount"/>
        /// </summary>
        int RenderedFrameCount { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.frameCount"/>
        /// </summary>
        int FrameCount { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.timeScale"/>
        /// </summary>
        float TimeScale { get; set; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.maximumParticleDeltaTime"/>
        /// </summary>
        float MaximumParticleDeltaTime { get; set; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.smoothDeltaTime"/>
        /// </summary>
        float SmoothDeltaTime { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.maximumDeltaTime"/>
        /// </summary>
        float MaximumDeltaTime { get; set; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.fixedDeltaTime"/>
        /// </summary>
        float FixedDeltaTime { get; set; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.fixedUnscaledDeltaTime"/>
        /// </summary>
        float FixedUnscaledDeltaTime { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.unscaledDeltaTime"/>
        /// </summary>
        float UnscaledDeltaTime { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.fixedUnscaledTimeAsDouble"/>
        /// </summary>
        double FixedUnscaledTimeAsDouble { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.fixedUnscaledTime"/>
        /// </summary>
        float FixedUnscaledTime { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.unscaledTimeAsDouble"/>
        /// </summary>
        double UnscaledTimeAsDouble { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.unscaledTime"/>
        /// </summary>
        float UnscaledTime { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.fixedTimeAsDouble"/>
        /// </summary>
        double FixedTimeAsDouble { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.fixedTime"/>
        /// </summary>
        float FixedTime { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.deltaTime"/>
        /// </summary>
        float DeltaTime { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.timeSinceLevelLoadAsDouble"/>
        /// </summary>
        double TimeSinceLevelLoadAsDouble { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.timeSinceLevelLoad"/>
        /// </summary>
        float TimeSinceLevelLoad { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.timeAsDouble"/>
        /// </summary>
        double TimeAsDouble { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.time"/>
        /// </summary>
        float Time { get; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.captureFramerate"/>
        /// </summary>
        int CaptureFramerate { get; set; }

        /// <summary>
        /// <inheritdoc cref="UnityTime.inFixedTimeStep"/>
        /// </summary>
        bool InFixedTimeStep { get; }
    }
}