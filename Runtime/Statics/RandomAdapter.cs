namespace UnityEngine
{
    /// <summary>
    /// <inheritdoc/>
    /// Adapter class for Unity <see cref="Random"/> static class.
    /// </summary>
    public sealed class RandomAdapter : IRandom
    {
        public Quaternion RotationUniform => Random.rotationUniform;

        public Quaternion Rotation => Random.rotation;

        public Vector3 OnUnitSphere => Random.onUnitSphere;

        public Vector2 InsideUnitCircle => Random.insideUnitCircle;

        public Random.State State
        {
            get => Random.state;
            set => Random.state = value;
        }

        public float Value => Random.value;

        public Vector3 InsideUnitSphere => Random.insideUnitSphere;

        public Color ColorHSV(
            float hueMin, float hueMax, float saturationMin, float saturationMax,
            float valueMin, float valueMax, float alphaMin, float alphaMax) =>
            Random.ColorHSV(
                hueMin, hueMax, saturationMin, saturationMax,
                valueMin, valueMax, alphaMin, alphaMax);

        public Color ColorHSV() => Random.ColorHSV();

        public Color ColorHSV(float hueMin, float hueMax) => Random.ColorHSV(hueMin, hueMax);

        public Color ColorHSV(
            float hueMin, float hueMax,
            float saturationMin, float saturationMax) =>
            Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax);

        public Color ColorHSV(
            float hueMin, float hueMax, float saturationMin,
            float saturationMax, float valueMin, float valueMax) =>
            Random.ColorHSV(
            hueMin, hueMax, saturationMin,
            saturationMax, valueMin, valueMax);

        public void InitState(int seed) => Random.InitState(seed);

        public int Range(int minInclusive, int maxExclusive) =>
            Random.Range(minInclusive, maxExclusive);

        public float Range(float minInclusive, float maxInclusive) =>
            Random.Range(minInclusive, maxInclusive);

        public int Range(params int[] list)
        {
            // Gets a random number btw 0 and (list.Length - 1)
            int randomIndex = Random.Range(0, list.Length);
            return list[randomIndex];
        }

        public float Range(params float[] list)
        {
            // Gets a random number btw 0 and (list.Length - 1)
            int randomIndex = Random.Range(0, list.Length);
            return list[randomIndex];
        }
    }
}