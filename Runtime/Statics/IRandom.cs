namespace UnityEngine
{
    /// <summary>
    /// Provider interface for a <see cref="Random"/>.
    /// </summary>
    public interface IRandom
    {
        /// <summary>
        /// <inheritdoc cref="Random.rotationUniform"/>
        /// </summary>
        Quaternion RotationUniform { get; }

        /// <summary>
        /// <inheritdoc cref="Random.rotation"/>
        /// </summary>
        Quaternion Rotation { get; }

        /// <summary>
        /// <inheritdoc cref="Random.onUnitSphere"/>
        /// </summary>
        Vector3 OnUnitSphere { get; }

        /// <summary>
        /// <inheritdoc cref="Random.onUnitSphere"/>
        /// </summary>
        Vector2 InsideUnitCircle { get; }

        /// <summary>
        /// <inheritdoc cref="Random.state"/>
        /// </summary>
        Random.State State { get; set; }

        /// <summary>
        /// <inheritdoc cref="Random.value"/>
        /// </summary>
        float Value { get; }

        /// <summary>
        /// <inheritdoc cref="Random.insideUnitSphere"/>
        /// </summary>
        Vector3 InsideUnitSphere { get; }

        /// <summary>
        /// <inheritdoc cref="Random.ColorHSV(float, float, float, float, float, float, float, float)"/>
        /// </summary>
        Color ColorHSV(
            float hueMin, float hueMax, float saturationMin, float saturationMax,
            float valueMin, float valueMax, float alphaMin, float alphaMax);

        /// <summary>
        /// <inheritdoc cref="Random.ColorHSV"/>
        /// </summary>
        Color ColorHSV();

        /// <summary>
        /// <inheritdoc cref="Random.ColorHSV(float, float)"/>
        /// </summary>
        Color ColorHSV(float hueMin, float hueMax);

        /// <summary>
        /// <inheritdoc cref="Random.ColorHSV(float, float, float, float)"/>
        /// </summary>
        Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax);

        /// <summary>
        /// <inheritdoc cref="Random.ColorHSV(float, float, float, float, float, float)"/>
        /// </summary>
        Color ColorHSV(
            float hueMin, float hueMax, float saturationMin,
            float saturationMax, float valueMin, float valueMax);

        /// <summary>
        /// <inheritdoc cref="Random.InitState(int)"/>
        /// </summary>
        void InitState(int seed);

        /// <summary>
        /// <inheritdoc cref="Random.Range(int, int)"/>
        /// </summary>
        int Range(int minInclusive, int maxExclusive);

        /// <summary>
        /// <inheritdoc cref="Random.Range(float, float)"/>
        /// </summary>
        float Range(float minInclusive, float maxInclusive);

        /// <summary>
        /// Returns a random number from the given list.
        /// </summary>
        /// <param name="list">An integer list.</param>
        /// <returns>A random int number.</returns>
        int Range(params int[] list);

        /// <summary>
        /// Returns a random number from the given list.
        /// </summary>
        /// <param name="list">A float list.</param>
        /// <returns>A random float number.</returns>
        float Range(params float[] list);
    }
}