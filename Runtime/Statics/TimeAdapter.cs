using UnityTime = UnityEngine.Time;

namespace UnityEngine
{
    /// <summary>
    /// <inheritdoc/>
    /// Adapter class for <see cref="UnityEngine.Time"/> static class.
    /// </summary>
    public sealed class TimeAdapter : ITime
    {
        public float CaptureDeltaTime
        {
            get => UnityTime.captureDeltaTime;
            set => UnityTime.captureDeltaTime = value;
        }

        public double RealtimeSinceStartupAsDouble => UnityTime.realtimeSinceStartupAsDouble;

        public float RealtimeSinceStartup => UnityTime.realtimeSinceStartup;

        public int RenderedFrameCount => UnityTime.renderedFrameCount;

        public int FrameCount => UnityTime.frameCount;

        public float TimeScale
        {
            get => UnityTime.timeScale;
            set => UnityTime.timeScale = value;
        }

        public float MaximumParticleDeltaTime
        {
            get => UnityTime.maximumParticleDeltaTime;
            set => UnityTime.maximumParticleDeltaTime = value;
        }

        public float SmoothDeltaTime => UnityTime.smoothDeltaTime;

        public float MaximumDeltaTime
        {
            get => UnityTime.maximumDeltaTime;
            set => UnityTime.maximumDeltaTime = value;
        }

        public float FixedDeltaTime
        {
            get => UnityTime.fixedDeltaTime;
            set => UnityTime.fixedDeltaTime = value;
        }

        public float FixedUnscaledDeltaTime => UnityTime.fixedUnscaledDeltaTime;

        public float UnscaledDeltaTime => UnityTime.unscaledDeltaTime;

        public double FixedUnscaledTimeAsDouble => UnityTime.fixedUnscaledTimeAsDouble;

        public float FixedUnscaledTime => UnityTime.fixedUnscaledTime;

        public double UnscaledTimeAsDouble => UnityTime.unscaledTimeAsDouble;

        public float UnscaledTime => UnityTime.unscaledTime;

        public double FixedTimeAsDouble => UnityTime.fixedTimeAsDouble;

        public float FixedTime => UnityTime.fixedTime;

        public float DeltaTime => UnityTime.deltaTime;

        public double TimeSinceLevelLoadAsDouble => UnityTime.timeSinceLevelLoadAsDouble;

        public float TimeSinceLevelLoad => UnityTime.timeSinceLevelLoad;

        public double TimeAsDouble => UnityTime.timeAsDouble;

        public float Time => UnityTime.time;

        public int CaptureFramerate
        {
            get => UnityTime.captureFramerate;
            set => UnityTime.captureFramerate = value;
        }

        public bool InFixedTimeStep => UnityTime.inFixedTimeStep;
    }
}