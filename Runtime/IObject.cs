namespace UnityEngine
{
    /// <summary>
    /// Provider interface for a <see cref="Object"/>.
    /// </summary>
    public interface IObject
    {
        /// <summary>
        /// <inheritdoc cref="Object.hideFlags"/>
        /// </summary>
        HideFlags HideFlags { get; set; }

        /// <summary>
        /// <inheritdoc cref="Object.name"/>
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// <inheritdoc cref="Object.Equals(object)"/>
        /// </summary>
        bool Equals(object other);

        /// <summary>
        /// <inheritdoc cref="Object.GetHashCode"/>
        /// </summary>
        int GetHashCode();

        /// <summary>
        /// <inheritdoc cref="Object.GetInstanceID"/>
        /// </summary>
        int GetInstanceID();

        /// <summary>
        /// <inheritdoc cref="Object.ToString"/>
        /// </summary>
        string ToString();
    }
}