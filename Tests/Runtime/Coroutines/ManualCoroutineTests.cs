using UnityEngine;
using NUnit.Framework;
using System.Collections;

namespace ActionCode.Adapters
{
    public class ManualCoroutineTests
    {
        public class BaseManualCoroutineTests
        {
            public ManualCoroutine Coroutine { get; private set; }

            public int CurrentCount { get; private set; }

            [SetUp]
            public void Setup()
            {
                Coroutine = new ManualCoroutine();
            }

            public IEnumerator EmptyCoroutine()
            {
                yield return null;
            }

            public IEnumerator CountCoroutine(int final)
            {
                CurrentCount = 0;
                while (CurrentCount < final)
                {
                    CurrentCount++;
                    yield return null;
                }
            }
        }

        public class IsRunning : BaseManualCoroutineTests
        {
            [Test]
            public void Returns_True_When_Coroutine_Was_Started()
            {
                Coroutine.StartCoroutine(EmptyCoroutine());
                Assert.IsTrue(Coroutine.IsRunning);
            }

            [Test]
            public void Returns_False_When_Coroutine_Was_Stopped()
            {
                var coroutine = EmptyCoroutine();

                Coroutine.StartCoroutine(coroutine);
                Coroutine.StopCoroutine(coroutine);

                Assert.IsFalse(Coroutine.IsRunning);
            }
        }

        public class Current : BaseManualCoroutineTests
        {
            [Test]
            public void Returns_Current_Routine()
            {
                var routine = EmptyCoroutine();
                Coroutine.StartCoroutine(routine);
                Assert.AreSame(routine.Current, Coroutine.Current);
            }
        }

        public class MoveNext : BaseManualCoroutineTests
        {
            [Test]
            public void Moves_Coroutine_Once()
            {
                var routine = CountCoroutine(final: 1);

                Coroutine.StartCoroutine(routine);
                Coroutine.MoveNext();

                Assert.AreEqual(1, CurrentCount);
            }

            [Test]
            public void Moves_Coroutine_N_Times()
            {
                var n = 10;
                var routine = CountCoroutine(final: n);

                Coroutine.StartCoroutine(routine);
                for (int i = 0; i < n; i++)
                {
                    Coroutine.MoveNext();
                }

                Assert.AreEqual(n, CurrentCount);
            }
        }

        public class MoveToEnd : BaseManualCoroutineTests
        {
            [Test]
            public void Moves_Coroutine_To_End()
            {
                var n = 10;
                var routine = CountCoroutine(final: n);

                Coroutine.StartCoroutine(routine);
                Coroutine.MoveToEnd();

                Assert.AreEqual(n, CurrentCount);
            }
        }

        public class StartCoroutine : BaseManualCoroutineTests
        {
            [Test]
            public void Sets_Routine()
            {
                Coroutine.StartCoroutine(EmptyCoroutine());
                Assert.IsNotNull(Coroutine.Routine);
            }
        }

        public class StopCoroutine : BaseManualCoroutineTests
        {
            [Test]
            public void Destroys_Routine_When_Same_Routine()
            {
                var routine = EmptyCoroutine();

                Coroutine.StartCoroutine(routine);
                Coroutine.StopCoroutine(routine);

                Assert.IsNull(Coroutine.Routine);
            }

            [Test]
            public void Doesnt_Destroy_Routine_When_Different_Routine()
            {
                var routine1 = EmptyCoroutine();
                var routine2 = EmptyCoroutine();

                Coroutine.StartCoroutine(routine1);
                Coroutine.StopCoroutine(routine2);

                Assert.IsNotNull(Coroutine.Routine);
            }
        }

        public class StopAllCoroutines : BaseManualCoroutineTests
        {
            [Test]
            public void Destroys_Routine()
            {
                Coroutine.StartCoroutine(EmptyCoroutine());
                Coroutine.StopAllCoroutines();

                Assert.IsNull(Coroutine.Routine);
            }
        }
    }
}