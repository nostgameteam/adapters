# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-12-11
### Added
- AudioSource Provider and Adapter
- Behaviour Provider and Adapter
- SphereCollider and Circle2DCollider Adapters
- BoxCollider and BoxCollider2D Adapters
- Collider Provider and Collider Factory
- Rigidbody Provider, Rigidbody, Rigidbody2D Adapters and Rigidbody Factory
- Transform Provider and Adapter
- Component Provider and Adapter
- ManualCoroutine
- Coroutine Provider and Adapter
- Time Provider and Adapter
- Random Provider and Adapter
- Object Provider and Adapter
- CHANGELOG
- README
- Initial commit

[Unreleased]: https://bitbucket.org/nostgameteam/providers/branches/compare/master%0D1.0.0
[1.0.0]: https://bitbucket.org/nostgameteam/providers/src/1.0.0/