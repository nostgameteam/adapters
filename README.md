# Adapters

* Adapters and Providers classes for Unity
* Unity minimum version: **2018.3**
* Current version: **1.0.0**
* License: **MIT**

## Summary

This package contains Providers interfaces used on Adapter classes. 
With it, uncoupled code can be created and Unit Tests are easier.

## Example

Suppose we have this Player class:

```csharp
public class Player
{
    public float jumpForce = 12f;

    private readonly Rigidbody2D body;

    public Player(Rigidbody2D body)
    {
        this.body = body;    		
    }

    public void Jump()
    {
        var force = Vector2.up * jumpForce;
        body.AddForce(force);
    }
}
```

How can we test if the Jump function will add a force into the local rigidbody using ```jumpForce```?

This would be difficult since this class is coupled with ```Rigidbody2D``` and Unity does not provide any abstraction we can use.

### Alternative Player Class

Using this package you can refactor the Player class like this:

```csharp
using UnityEngine;

public class Player
{
    public float jumpForce = 12f;

    private readonly IRigidbody body;

    public Player(IRigidbody body)
    {
        this.body = body;
    }

    public void Jump()
    {
        var force = Vector2.up * jumpForce;
        body.AddForce(force);
    }
}
```

Now the Player class is loosely coupled with rigidbody since it uses an ```IRigidbody``` provider interface.

Inside a MonoBehaviour you can instantiate the Player passing the Rigidbody adapter: 

```csharp
public Rigidbody2D body;

private void Awake()
{
    var player = new Player(new Rigidbody2DAdapter(body));
    // alternately you can use the RigidbodyAdapterFactory to get the right Rigidbody adapter instance
    // var player = new Player(RigidbodyAdapterFactory.Create(gameObject));
}
```

And this make easier to test the Player class:

```csharp
using NSubstitute;
using UnityEngine;
using NUnit.Framework;

public class PlayerTests
{
    public Player Player { get; private set; }
    public IRigidbody Body { get; private set; }

    [SetUp]
    public void Setup()
    {
        Body = Substitute.For<IRigidbody>();
        Player = new Player(Body);
    }

    [Test]
    public void Jump_Adds_Up_Force_Into_Rigidbody()
    {
        var expectedForce = Vector2.up * Player.jumpForce;

        Player.Jump();

        Body.Received().AddForce(expectedForce);
    }
}
```

You can check all the currently Providers and Adapters on the [CHANGELOG](https://bitbucket.org/nostgameteam/adapters/src/main/CHANGELOG.md).

## Installation

### Using the Package Registry Server

Follow the instructions inside [here](https://cutt.ly/ukvj1c8) and the package **ActionCode-Adapters** 
will be available for you to install using the **Package Manager** windows.

### Using the Git URL

You will need a **Git client** installed on your computer with the Path variable already set. 

Use the **Package Manager** "Add package from git URL..." feature or add manually this line inside `dependencies` attribute: 

```json
"com.actioncode.adapters":"https://bitbucket.org/nostgameteam/adapters.git"
```

---

**Hyago Oliveira**

[BitBucket](https://bitbucket.org/HyagoGow/) -
[LinkedIn](https://www.linkedin.com/in/hyago-oliveira/) -
<hyagogow@gmail.com>